# all()       | Return True if all elements of the tuple are true (or if the tuple is empty).
# any()       | Return True if any element of the tuple is true. if the tuple is empty, return False.
# enumerate() | Return an enumerate object. It contains the index and the value of all the items of tuple as pairs.
# len()       | Return the length (the number of items) in the tuple.
# max()       | Return the largest item in the tuple.
# min()       | Return the smallest item in the tuple
# sorted()    | Take elements in the tuple and return a new sorted list(does not sort the tuple itself).
# sum()       | Return the sum of all elements in the tuple.
# tuple()     | Convert an iterable (list, string, set, dictionary) to a tuple.

# Creating an empty tuple
print('============================')
tuple1 = ()
print(tuple1)

# Creating tuples with integer elements
print('============================')
tuple2 = (1, 2, 3)
print(tuple2)

# Tuple with mixed datatypes
print('============================')
tuple3 = (101, "Anirban", 20000.00, "HR Dept")
print(tuple3)

# Creation of nested tuple
print('============================')
tuple4 = ("points", [1, 4, 3], (7, 8, 6))
print(tuple4)

# Tuple can be creted without any parentheses
# also called tuple packing
print('============================')
tuple5 = 101, 'Anirban', 20000.00, 'HR Dept'
print(tuple5)

# Tuple unpacking is also possible
print('============================')
empid, empname, empsal, empdept = tuple5
print(empid)
print(empname)
print(empsal)
print(empdept)
print(type(tuple5))

# Accessing elements in a tuple
print('============================')
tuple1 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
print(tuple1)
print(tuple1[1])
print(tuple1[3])
print(tuple1[5])

# Nested tuple
print('============================')
nest_tuple2 = ('point', [1, 3, 4], (8, 7, 3))
print(nest_tuple2)
print(nest_tuple2[0][3])
print(nest_tuple2[1][2])
print(nest_tuple2[2][2])

# Slicing tuple contents
print('============================')
tuple1 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
print(tuple1[1:3])
print(tuple1[:-3])
print(tuple1[3:])
print(tuple1[:])

# Tuple elements are immutable
print('============================')
tuple1 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
print(tuple1)
# tuple1[2] = 'x' # error

# Tuples can be reassigned
print('============================')
tuple1 = ('g', 'o', 'o', 'd', 'b', 'y', 'e')
print(tuple1)

# Concatenation of tuples
print('============================')
tuple2 = ('w', 'e', 'l')
tuple3 = ('c', 'o', 'm', 'e')
print(tuple2)
print(tuple3)
print(tuple2 + tuple3)

print(('again', ) * 4) 

# Deletion operation on a tuple
print('============================')
tuple4 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')

# As immutable so elements can not be delted
# del tuple4[2]

# But deleting entire tuple is fine
print('============================')
del tuple4
# print(tuple4) # tuple4 is not defined anymore

# Tuple methods
print('============================')
tuple5 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
print(tuple5.count('e'))
print(tuple5.index('c'))

# Tuple operations
print('============================')
tuple6 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')

# Membership
print('============================')
print('c' in tuple6)
print('c' not in tuple6)
print('a' in tuple6)
print('a' not in tuple6)

# Iteration through tuple elements
print('============================')
tuple6 = ('w', 'e', 'l', 'c', 'o', 'm', 'e')
for letters in tuple6:
    print('letter is -> ', letters)

# Built-in functions with tuple
print('============================')
tuple7 = (22, 33, 55, 44, 77, 66, 11)
print(tuple7)

print(max(tuple7))
print(min(tuple7))
print(sorted(tuple7))
print(len(tuple7))