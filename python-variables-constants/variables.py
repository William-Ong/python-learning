# Declaring and assigning value to constants
print('============================')
PI = 3.14
GRAVITY = 9.8
print(PI)

# Declaring and assigning value to a variable
print('============================')
a = "Apple"
print(a)

# Changing value of a variable
print('============================')
a = "Aeroplane"
print(a)

a = 100
print(a)

# Assigning Multiple values to a variable
print('============================')
b, c, d = 1, 2.5, "Hello"
print(b, c, d)

# Assigning same value to multiple variables 
print('============================')
b = c = d = 5
print(b, c, d)