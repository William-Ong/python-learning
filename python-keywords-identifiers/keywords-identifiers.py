# True False
print('============================')
print(5 == 5)
print(5 > 5)

# None
print('============================')
print(None == 0)
print(None == False)
print(None == [])
print(None == None)

def a_void_function():
    a = 1
    b = 2
    c = a + b

x = a_void_function()
print(x)

# and, or, not
print('============================')
print(True and False)
print(True or False)
print(not False)

# as
print('============================')
import math as myMath
print(myMath.cos(myMath.pi))

# assert
print('============================')
assert 5 == 5

# break
print('============================')
for i in range(1, 11):
    if i == 5:
        break
    print(i)

# continue
print('============================')
for i in range(1, 8):
    if i == 5:
        continue
    print(i)

# class
print('============================')
class ExampleClass:
    def function1(parameters):
        print("function1() executing...")
    def function2(parameters):
        print("function2() executing...")
ob1 = ExampleClass()
ob1.function1()
ob1.function2()

# def
print('============================')
def function_name(parameters):
    pass
function_name("baba booey")

# del
print('============================')
a = 10
print(a)
del a
# print(a) --> throws NameError

# if..elif..else
print('============================')
num = 2
if num == 1: 
    print('One')
elif num == 2:
    print('Two')
else:
    print('Amogus')

# try...raise...catch...finally
print('============================')
try:
    x = 9
    raise ZeroDivisionError
except ZeroDivisionError:
    print("Division cannot be performed")
finally:
    print("Execution Successfully")

# for
print('============================')
for i in range(1, 10):
    print(i)

# from..import
print('============================')
import math
from math import cos
print(cos(10))

# global
print('============================')
globvar = 10
def read1():
    print(globvar)
def write1():
    global globvar
    globvar = 5
def write2():
    globvar = 15
read1()
write1()
read1()
write2()
read1()

# in
print('============================')
a = [1, 2, 3, 4]
print(4 in a) # True
print(44 in a) # False

# is
print('============================')
print(True is True)

# lambda
print('============================')
a = lambda x: x*2
for i in range(1, 6):
    print(a(i))

# nonlocal
print('============================')
def outer_function():
    a = 5
    def inner_function():
        nonlocal a 
        a = 10
        print("Inner function: ", a)
    inner_function()
    print("Outer function: ", a)

outer_function()

# pass
print('============================')
def function(args):
    pass

# return
print('============================')
def func_return():
    a = 10
    return a
print(func_return())

# while
print('============================')
i = 5
while(i > 0):
    print(i)
    i -= 1

# with
print('============================')
with open('example.txt', 'w') as my_file:
    my_file.write('Hello world!')

# yield
print('============================')
def generator():
    for i in range(6):
        yield i*i

g = generator()
for i in g:
    print(i)