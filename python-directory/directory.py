print('============================')
import os 
print(os.getcwd()) # Returns the present working directory
print(os.getcwdb()) # Returns the present working directory as a byte object

print("Changing directory to python-directory...")
os.chdir('/mnt/c/Users/William Ong/Documents/Workspace/python-learning/python-directory') # Use to change directory
print(os.listdir())
print(os.getcwd())

# Used to make a new directory
print("Making new directory...")
os.mkdir('Test')

# Used to rename a directory
print("Renaming directory...")
os.rename('Test', 'New_One')

# Adding an empty file
print("Adding Text.txt...")
os.mknod("Test.txt")

# Removing a file and directory
print("Removing Text.txt...")
os.remove('Test.txt') # Use to remove a file
print("Removing New_One directory...")
os.rmdir('New_One') # Use to remove a directory

print("Changing directory to python-learning...")
os.chdir('/mnt/c/Users/William Ong/Documents/Workspace/python-learning')