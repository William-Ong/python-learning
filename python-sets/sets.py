# add()                 | Add an element to a set
# clear()               | Remove all elements from a set
# copy()                | Return a shallow copy of a set
# difference()          | Return the difference of two or more sets as a new set
# difference_update()   | Remove all elements of another set from this set
# discard()             | Remove an element from set if it is a member. (Do nothing if the element is not in set)
# intersection()        | Return the intersection of two sets as a new set
# intersection_update() | Update the set with the intersection of itself and another
# isdisjoint()          | Return True if two sets have a null intersection
# issubset()            | Return True if another set contains this set
# all()                 | Return True if all elements of the set are true (or if the set is empty).
# any()                 | Return True if any element of the set is true. If the set is empty, return False.
# enumerate()           | Return an enumerate object. It contains the index and value of all the items of set as a pair.
# len()                 | Return the length (the number of items) of the set.
# max()                 | Return the largest item in the set.
# min()                 | Return the smallest item in the set.
# sorted()              | Return a new sorted list from elements in the set (does not sort the set itself).
# sum()                 | Return the sum of all elements in the set.

# Creating a set of integers
print('============================')
my_set1 = {11, 33, 66, 55, 44, 22}
print(my_set1)

# Set of mixed datatypes
print('============================')
my_set2 = {101, 'Agnibha', (21, 2, 1994)}
print(my_set2)

# Duplicate values are not allowed
print('============================')
my_set3 = {11, 22, 33, 33, 44, 22}
print(my_set3)

# Set cannot have mutable items
# my_set4 = {1, 2, [3, 4]}

# We can make set from a list
print('============================')
my_set5 = set([1, 2, 3, 2])
print(my_set5)
print(type(my_set5))

# We can make list from a set
print('============================')
my_list1 = list({11, 22, 33, 44})
print(my_list1)
print(type(my_list1))

# Operations on sets
print('============================')
my_set1 = {11, 33, 44, 66, 55}
print(my_set1)

# 'set' object does no support indexing
# my_set1[0] # Error

# Adding an element
print('============================')
my_set1.add(77)
print(my_set1)

# Add multiple elements
print('============================')
my_set1.update([88, 99, 22])
print(my_set1)

# Add list and set
print('============================')
my_set1.update([100, 102], {103, 104, 105})
print(my_set1)

# Remove and discard
print('============================')
my_set1 = {11, 33, 44, 55, 66}
print(my_set1)

# Discard an element which is not present, no error
print('============================')
my_set1.discard(4)
print(my_set1)

# Remove an element which is not present, error raised
print('============================')
# my_set1.remove(6)
print(my_set1)

# Discard an element
print('============================')
my_set1.discard(44)
print(my_set1)
my_set1.remove(55)
print(my_set1)

# Using pop()
print('============================')
my_set1 = {11, 33, 44, 55, 66}
print(my_set1)

# Pop an element
print('============================')
print(my_set1.pop())

# Pop another element
print('============================')
print(my_set1.pop())
print(my_set1)

# Clearing
print('============================')
my_set1.clear()
print(my_set1)

# Set operations - union
print('============================')
my_set1 = {0, 1, 2, 3, 4, 5}
my_set2 = {4, 5, 6, 7, 8, 9}
print(my_set1)
print(my_set2)

# Use | operator for union
print('============================')
print(my_set1 | my_set2)
print(my_set2 | my_set1)
print(my_set1.union(my_set2))
print(my_set2.union(my_set1))

# Set operations - intersection
print('============================')
my_set1 = {0, 1, 2, 3, 4, 5}
my_set2 = {4, 5, 6, 7, 8, 9}
print(my_set1)
print(my_set2)

# Use & operator for intersection
print('============================')
print(my_set1 & my_set2)
print(my_set2 & my_set1)
print(my_set1.intersection(my_set2))
print(my_set2.intersection(my_set1))

# Set operations - set difference
print('============================')
my_set1 = {0, 1, 2, 3, 4, 5}
my_set2 = {4, 5, 6, 7, 8, 9}
print(my_set1)
print(my_set2)

# Use - operator for set difference
print('============================')
print(my_set1 - my_set2)
print(my_set2 - my_set1)
print(my_set1.difference(my_set2))
print(my_set2.difference(my_set1))

# Set operations - symmetric difference
print('============================')
my_set1 = {0, 1, 2, 3, 4, 5}
my_set2 = {4, 5, 6, 7, 8, 9}
print(my_set1)
print(my_set2)

# Use ^ operator for symmetric difference
print('============================')
print(my_set1 ^ my_set2)
print(my_set2 ^ my_set1)
print(my_set1.symmetric_difference(my_set2))
print(my_set2.symmetric_difference(my_set1))

# Set membership
print('============================')
my_set1 = {0, 1, 2, 3, 4, 5}
print(2 in my_set1)
print(6 in my_set1)
print(2 not in my_set1)
print(6 not in my_set1)

# Iterating through a set
print('============================')
for letter in set("welcome"):
    print(letter)

# Built-in functions with set
print('============================')
my_set1 = {0, 1, 2, 3, 4, 5}
print(len(my_set1))
print(max(my_set1))
print(min(my_set1))
print(sorted(my_set1))

# Python frozenset
# Frozenset is a new class that has the characteristics
# of a set, but its elements cannot be changed once assigned
# While tuples are immutable list, frozensets are immutable sets.
print('============================')
my_set1 = frozenset([1, 2, 3, 4])
my_set2 = frozenset([3, 4, 5, 6])
print(my_set1)
print(my_set2)
print(my_set1.difference(my_set2))
print(my_set1.union(my_set2))
print(my_set1.intersection(my_set2))
print(my_set1.symmetric_difference(my_set2))
# my_set1.add(10)