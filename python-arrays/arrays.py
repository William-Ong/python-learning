# append()  | Add element to the end of the list
# extend()  | Extend all elements of a list to another list
# insert()  | Insert an element at another index
# remove()  | Remove an element from the list
# pop()     | Remove and return element at a given index
# clear()   | Remove all elements from the list
# index()   | Return the index of the first matched element
# count()   | Count the number of elements passed as an argument
# sort()    | Sort the elements in ascending order by default
# reverse() | Reverse ordered element in a list
# copy()    | Return a copy of elements in a list

# Defining and declaring an array
print('============================')
arr = [10, 20, 30, 40, 50]
print(arr)

# Accessing Array elements
print('============================')
print(arr[0])
print(arr[1])
print(arr[2])
print(arr[-1]) # Negative Indexing
print(arr[-2])

brands = ["Coke", "Apple", "Google", "Microsoft", "Toyota"]
print(brands)

# Finding the length of the array
print('============================')
num_brands = len(brands)
print(num_brands)

# Adding an element to an array using append()
print('============================')
brands.append("Intel")
print(brands)

# Removing elements from an array
print('============================')
colors = ["violet", "indigo", "blue", "green", "yellow", "orange", "red"]
del colors[4]
colors.remove("blue")
colors.pop(3)
print(colors)

# Modifying elements of an array using indexing
print('============================')
fruits = ["Apple", "Banana", "Mango", "Grapes", "Orange"]
fruits[1] = "Pineapple"
fruits[-1] = "Guava"
print(fruits)

# Concatenating two arrays using the + operator
print('============================')
concat = [1, 2, 3]
concat = concat + [4, 5, 6]
print(concat)

# Repeating element in an array
print('============================')
repeat = ["a"]
repeat = repeat * 5
print(repeat)

# Slicing an array 
print('============================')
fruits = ["Apple", "Banana", "Mango", "Grapes", "Orange"]
print(fruits[1:4])
print(fruits[ : 3])
print(fruits[-4:])
print(fruits[-3:-1])

# Delcaring and definining multidimensional array
print('============================')
multd = [[1,2], [3, 4], [5, 6], [7, 8]]
print(multd)
print(multd[0])
print(multd[3])
print(multd[2][1])
print(multd[3][0])